import java.util.Scanner;

public class SegitigaBintang {
    public static void main(String[] args) {
        System.out.print("Enter rows: ");
        Scanner scanner = new Scanner(System.in);
        int rows = scanner.nextInt();
        System.out.println(" ");
        for(int i=1;i<=rows;i++){
            for(int j=rows;j>=i;j--){
                System.out.print(" ");
            }
            for(int j=1;j<=i;j++){
                System.out.print("* ");
            }
            System.out.println(" ");
        }
    }
}

